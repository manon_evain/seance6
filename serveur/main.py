from typing import Optional
from serveur.guess import Guess
from serveur.mot import Mot
from fastapi import FastAPI
import uvicorn

app = FastAPI()

# TODO : déplacer getMots dans un autre fichier que le main
def getMots():
    return ["arbre","maison","voiture"]

@app.get("/init")
def init_guess(mot : Mot):
    tailles = len(mot.caracteres)
    mot_cache = ''
    for i in range(0, tailles):
        mot_cache = mot_cache + "-"
    return Guess(Mot(id, mot_cache), 0, '')

@app.post("/guess")
def post_guess(guess_prec : Guess, lettre : str):
    lettres_faites = guess_prec.letter
    if lettre in lettres_faites: 
        return "La lettre à déjà été essayée"
    else: 
        lettres_faites = lettres_faites + lettre
        return Guess(Mot(id, '--------'), 1, lettres_faites)

@app.get("/mots")
def read_root():
    return {"mots":getMots()}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

@app.post("/mot")
def add_mot(mot: Mot):
    print(mot.caracteres)


if __name__ == "__main__" :
    uvicorn.run(app, host="localhost", port = "5000", log_level="info")