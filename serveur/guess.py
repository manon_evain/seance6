from serveur.mot import Mot
from pydantic import BaseModel

class Guess(BaseModel):
    mot:Mot
    erreurs:int
    letter:str
    def __init__(self,mot,erreurs,letter):
        self.mot=mot
        self.erreurs=erreurs
        self.letter=letter
